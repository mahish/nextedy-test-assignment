import React, { useEffect, useRef, useState } from "react";
import { LocalStorage } from "./services/local-storage";
import { getFilteredItems } from "./services/helpers";
import Filter from "./components/Filter";
import ResultTable from "./components/ResultTable";
import "./App.scss";

function App() {
  const url = "https://fakerapi.it/api/v1/books?_quantity=50&_seed=12456";

  const cache = useRef({});

  const [options, setOptions] = useState([]);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [dataToShow, setDataToShow] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!loading) return;

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        // 1. set original data
        cache.current[url] = data.data;

        // 2. parse data for select/filter options
        setOptions(
          data.data.map((item) => ({
            value: item.isbn,
            label: `${item.title}`,
          }))
        );

        // 3. check if stored selected options
        const json = LocalStorage.options;
        if (json && json.length) {
          setSelectedOptions(json);

          // 4. if yes then show filtered data only
          setDataToShow(getFilteredItems(data.data, json, ["isbn", "value"]));
        }
        // 4. if not then show all data
        else {
          setDataToShow(data.data);
        }
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, [loading]);

  // store selected options in localStorage
  useEffect(() => {
    if (loading) return;

    LocalStorage.options = selectedOptions;
  }, [loading, selectedOptions]);

  function onFilterChange(selectedItems) {
    // get options to set as a value of the filter
    setSelectedOptions(selectedItems);

    // filter original data based on selected items
    setDataToShow(
      selectedItems.length === 0
        ? cache.current[url]
        : getFilteredItems(cache.current[url], selectedItems, ["isbn", "value"])
    );
  }

  if (loading) {
    return <div className="App">Loading…</div>;
  } else if (cache.current[url]) {
    return (
      <div className="App">
        <Filter
          value={selectedOptions}
          data={options}
          onChange={onFilterChange}
        />
        {dataToShow.length > 0 && <ResultTable data={dataToShow} />}
        {dataToShow.length === 0 && <div>No results for the query…</div>}
      </div>
    );
  } else {
    return <div className="App">No data…</div>;
  }
}

export default App;

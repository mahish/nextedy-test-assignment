import React from "react";
import PropTypes from "prop-types";
import ResultItem from "../ResultItem";
import "./styles.scss";

const columns = [
  { name: "title", format: (value) => value },
  { name: "isbn", format: (value) => value },
  // {
  //   name: "price",
  //   format: (value) =>
  //     Intl.NumberFormat(navigator.language, {
  //       style: "currency",
  //       currency: "USD",
  //       currencyDisplay: "narrowSymbol",
  //     }).format(value),
  // },
  {
    name: "published",
    format: (value) =>
      Intl.DateTimeFormat(navigator.language, {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        // hour: "numeric",
        // minute: "2-digit",
      }).format(new Date(value)),
  },
];

const ResultTable = ({ data }) => {
  return (
    <div className="ResultTable">
      <div className="ResultTable__header">
        {columns.map((column) => (
          <div
            className="ResultTable__column"
            key={`ResultTable-header-${column.name}`}
          >
            {column.name}
          </div>
        ))}
      </div>

      <div className="ResultTable__items">
        {data.map((item) => (
          <ResultItem
            columns={columns}
            data={item}
            key={`ResultItem-${item.isbn}`}
          />
        ))}
      </div>
    </div>
  );
};

ResultTable.propTypes = {
  data: PropTypes.array.isRequired,
};

export default ResultTable;

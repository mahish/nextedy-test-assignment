import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";

const Filter = ({ value, data, onChange }) => {
  return (
    <Select
      value={value}
      options={data}
      onChange={onChange}
      isMulti={true}
      closeMenuOnSelect={true}
      isClearable={true}
      isSearchable={true}
      className="Filter"
      classNamePrefix="Filter"
      name="Shares"
    />
  );
};

Filter.propTypes = {
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.array.isRequired,
};

Filter.defaultProps = {};

export default Filter;

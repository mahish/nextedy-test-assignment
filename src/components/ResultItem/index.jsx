import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import "./styles.scss";

const ResultItem = ({ data, columns }) => {
  return (
    <div className="ResultItem">
      {columns.map((column) => (
        <div
          className={classnames(
            "ResultItem__column",
            `ResultItem__column--${column.name}`
          )}
          key={`ResultItem-${data.name}-${column.name}`}
        >
          {column.format(data[column.name])}
        </div>
      ))}
    </div>
  );
};

ResultItem.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.object.isRequired,
};

export default ResultItem;

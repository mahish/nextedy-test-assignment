export function getFilteredItems(data, filter, compare) {
  return data.filter((d) =>
    filter.some((f) => d[compare[0]] === f[compare[1]])
  );
}

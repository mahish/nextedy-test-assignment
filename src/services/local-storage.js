const LocalStorage = {
  get options() {
    const data = localStorage.getItem("options");
    return JSON.parse(data);
  },

  set options(data) {
    const json = JSON.stringify(data);
    localStorage.setItem("options", json);
  },
};

export { LocalStorage };
